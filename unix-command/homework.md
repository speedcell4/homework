# Homework

## 1. What the following script does?

```bash
export LOGFILE=$LOGDIRECTORY/${SCRIPT_NAME}.log
exec > >(tee $LOGFILE)
exec 2>&1
```


* Let `LOGDIRECTORY`, `SCRIPT_NAME` are initialized to
something.
* `LOGFILE=$LOGDIRECTORY/${SCRIPT_NAME}.log`
	* substitutes variables and produces a new string
* exec apply its arguments to the current shell

this is script redirect all the standard output and standard erro into `$LOGFILE`. for example.

```bash
➜  ~ mkdir nya
➜  ~ cd nya
➜  nya export LOGDIRECTORY=.
➜  nya export SCRIPT_NAME=info
➜  nya export LOGFILE=$LOGDIRECTORY/${SCRIPT_NAME}.log
➜  nya exec > >(tee $LOGFILE)
➜  nya exec 2>&1
➜  nya ls
info.log
➜  nya echo "hello, world"
hello, world
➜  nya cd miao
cd: no such file or directory: miao
➜  nya cat info.log
info.log
hello, world
cd: no such file or directory: miao
```

we can notice that all the output and error information are storaged in `info.log`. 

## 2. How to do `git commit --amend` without using `--amend` flag


assume there is a git repo like this

```bash
➜  ~ git init repo
Initialized empty Git repository in /Users/lambdell/repo/.git/
➜  ~ cd repo
➜  repo git:(master) echo "111" > 1.txt
➜  repo git:(master) ✗ echo "222" > 2.txt
➜  repo git:(master) ✗ echo "333" > 3.txt
➜  repo git:(master) ✗ ls
1.txt 2.txt 3.txt
```

we add and commit `1.txt` and `2.txt` firstly

```bash
➜  repo git:(master) ✗ git add 1.txt
➜  repo git:(master) ✗ git commit -m "add 1.txt"
[master (root-commit) e316832] add 1.txt
 1 file changed, 1 insertion(+)
 create mode 100644 1.txt
➜  repo git:(master) ✗ git add 2.txt
➜  repo git:(master) ✗ git commit -m "add 2.txt"
[master e3b19b7] add 2.txt
 1 file changed, 1 insertion(+)
 create mode 100644 2.txt
```

then if we wanna change this commit, we can use `reset` command

```bash
➜  repo git:(master) ✗ git reset --soft HEAD^
➜  repo git:(master) ✗ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   2.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	3.txt
```

in this way, we get back to the time before the last commit, and we can make some change then recommit.

```bash
➜  repo git:(master) ✗ git add 3.txt
➜  repo git:(master) ✗ git commit -m "add 2.txt and 3.txt"
[master 76d40c0] add 2.txt and 3.txt
 2 files changed, 2 insertions(+)
 create mode 100644 2.txt
 create mode 100644 3.txt
```

the log shows that we successfully changed the last commit.

```bash
* commit 76d40c0735956cd53fb0f1e3f5f12b89d0337e9f
| Author: Lambdell <speedcell4@gmail.com>
| Date:   Wed Jun 1 15:23:46 2016 +0900
|
|     add 2.txt and 3.txt
|
* commit e316832209f1046a14304685656ef93304ed1ca5
  Author: Lambdell <speedcell4@gmail.com>
  Date:   Wed Jun 1 15:15:13 2016 +0900

      add 1.txt
```
