for a in range(1, 10):
    for b in range(1, a + 1):
        print('{a} x {b} = {ans}'.format(a=a, b=b, ans=a * b), end=', ')
    print()
