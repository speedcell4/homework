not (1 == 1)
# False

2 == 1 + 1
# True

0.9 - 0.6 == 0.3
# False

True or True and False
# True