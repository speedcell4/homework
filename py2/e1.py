def copy_list(a):
    retval = []
    for item in a:
        retval.append(item)
    return retval


a = [2, 1, 4, 5, 3]
b = copy_list(a)

assert a == b
assert a is not b
