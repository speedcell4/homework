def replace(a):
    a[1:] = [0] * (len(a) - 1)

a = [3, 5, 2, 4, 2]
replace(a)

print(a)