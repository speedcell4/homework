def count(a):
    retval = {}
    for item in a:
        retval[item] = retval.get(item, 0) + 1
    return retval


a = count(['eggs', 'spam', 'spam', 'bacon'])
print(a)
print(sorted(a.items(), key=lambda item: item[1]))
