def get_max(a, b, c):
    def get_max_2(a, b):
        return a if a > b else b

    return get_max_2(a, get_max_2(b, c))


print(get_max(1, 2, 4))
