def get_even_numbers(n):
    retval = []
    for num in range(1, n + 1):
        if num % 2 == 0:
            retval.append(num)
    return retval


print(get_even_numbers(30))
