def get_index_of_max(a):
    max_val = max(a)
    return a.index(max_val)


print(get_index_of_max([2, 3, 41, 5]))
