def get_day(n):
    return [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
    ][n]


if __name__ == '__main__':
    import sys

    print(get_day(int(sys.argv[1])))
