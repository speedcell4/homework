def fib_loop(n):
    fib = [0, 1]
    while len(fib) <= n:
        fib.append(fib[-2] + fib[-1])
    return fib[n]


if __name__ == '__main__':
    import sys

    print(fib_loop(int(sys.argv[1])))
