from random import shuffle

cipher = list(range(26))
shuffle(cipher)

cipher_e = {chr(index + ord('a')): chr(key + ord('a')) for index, key in enumerate(cipher)}
cipher_d = {chr(key + ord('a')): chr(index + ord('a')) for index, key in enumerate(cipher)}


def encrypt(a):
    return ''.join(cipher_e[char] for char in a)


def decrypt(a):
    return ''.join(cipher_d[char] for char in a)


encrypted = encrypt('bara')
print(encrypted)
print(decrypt(encrypted))
