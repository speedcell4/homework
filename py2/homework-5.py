def quick_sort(a):
    if len(a) == 0:
        return a
    else:
        ls = [item for item in a if item < a[0]]
        gt = [item for item in a if item > a[0]]
        return quick_sort(ls) + [item for item in a if item == a[0]] + quick_sort(gt)


print(quick_sort([2, 5, 1, 2, 64, 645, 23]))
