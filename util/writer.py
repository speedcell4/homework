from pyknp import MList


def mrph_list_to_str(mrph_list: MList) -> str:
    return ''.join(mrph.midasi for mrph in mrph_list)
