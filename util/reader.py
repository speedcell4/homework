import sys
from typing import Iterator


def get_data() -> Iterator[str]:
    ans = []
    for line in sys.stdin:
        ans.append(line)
        if line.strip() == 'EOS':
            yield ''.join(ans)
            ans = []
