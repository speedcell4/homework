def cp(source_file, target_file):
    with open(source_file, 'r') as reader:
        with open(target_file, 'w') as writer:
            for line in reader:
                writer.write(line)


if __name__ == '__main__':
    import sys

    cp(sys.argv[1], sys.argv[2])