import math


def area(radius):
    return radius * radius * math.pi


print('{:.2f}'.format(area(3.5)))
