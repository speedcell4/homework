import re


def is_empty_re(string):
    return re.match(r'^\s+$', string) is not None


def is_empty_loop(string):
    for char in string:
        if char not in [' ', '\n', '\t']:
            return False
    return True


if __name__ == '__main__':
    print(is_empty_loop(' '))
