import gzip
import pickle
import re
from typing import List

from nltk import ngrams


def tokenize(sent: str) -> List[str]:
    return re.findall(r'\b\w+\b', sent.lower())


def get_data(filename: str = r'/share/text/WWW.en/txt.en/tsubame00/doc0000000000.txt.gz'):
    ignores = [
        re.compile(r'^<PAGE\s+URL='),
        re.compile(r'^</PAGE>'),
    ]

    with gzip.open(filename, 'rb') as handle:
        for line in handle:
            line = str(line, encoding='utf-8')
            if not any(ignore.match(line) for ignore in ignores):
                yield tokenize(line)


class LanguageModel(object):
    def __init__(self, sents):
        self.total = 0
        self.gram1 = {}
        self.gram2 = {}

        for sent in sents:
            self.total += len(sent)

            for gram in ngrams(sent, 1, right_pad_symbol='EOS'):
                self.gram1[gram] = self.gram1.get(gram, 0) + 1

            for gram in ngrams(sent, 2, right_pad_symbol='EOS'):
                self.gram2[gram] = self.gram2.get(gram, 0) + 1

    def estimate(self, sent: str) -> float:
        sent = tokenize(sent)
        gram1s = ngrams(sent, 1, right_pad_symbol='EOS')
        gram2s = ngrams(sent, 2, right_pad_symbol='EOS')

        prob = self.gram1.get((sent[0],), 0) / self.total
        for gram1, gram2 in zip(gram1s, gram2s):
            prob *= self.gram2.get(gram2, 0) / self.gram1.get(gram1, 1)

        return prob


def get_model(reload: bool = False) -> LanguageModel:
    try:
        if reload:
            raise FileNotFoundError()
        with open(r'model.pkl', 'rb') as reader:
            return pickle.load(reader)
    except FileNotFoundError:
        with open(r'model.pkl', 'wb') as writer:
            language_model = LanguageModel(get_data())
            pickle.dump(language_model, writer)
            return language_model


if __name__ == '__main__':
    print(get_model().estimate(r'The man is in the house.'))
