# coding: utf-8
import sys

from pyknp import KNP

knp = KNP()

for line in sys.stdin:
    result = knp.parse(line)
    for bnst in result.bnst_list():
        for mrph1, mrph2 in enumerate(bnst.mrph_list(), bnst.mrph_list()[1:]):
            if "<自立>" in mrph1:
                print(mrph1)
