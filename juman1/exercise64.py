from pyknp import Juman, Morpheme

from util.reader import get_data

juman = Juman()

for datum in get_data():
    infos = {}
    result = juman.result(datum)
    for mrph in result.mrph_list():  # type: Morpheme
        infos[mrph.genkei] = infos.get(mrph.genkei, 0) + 1
    print(' '.join(inf[0] for inf in sorted(infos.items(), key=lambda v: v[1])))
