import sys

from pyknp import Juman

juman = Juman()

for line in sys.stdin:
    print(' '.join(mrph.midasi for mrph in juman.analysis(line.strip())))
