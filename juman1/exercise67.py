from pyknp import Juman

from util.reader import get_data

juman = Juman()

for datum in get_data():
    result = juman.result(datum)
    for index, mrph in enumerate(result):
        if mrph.midasi == 'の' and result[index - 1].hinsi == '名詞' and result[index + 1].hinsi == '名詞':
            print('{} {} {}'.format(result[index - 1].midasi, mrph.midasi, result[index + 1].midasi))
