from pyknp import KNP

from util.reader import get_data

knp = KNP()

for datum in get_data():
    result = knp.result(datum)
    for bnst in result.bnst_list():
        if sum(1 if mrph.hinsi == '名詞' else 0 for mrph in bnst.mrph_list()) >= 2:
            print(''.join(mrph.midasi for mrph in bnst.mrph_list()))
