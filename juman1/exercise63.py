from typing import List

from pyknp import Juman, Morpheme

from util.reader import get_data

juman = Juman()

for datum in get_data():
    result = juman.result(datum)
    mrph_list = result.mrph_list()  # type: List[Morpheme]
    print(' '.join(mrph.genkei for mrph in mrph_list if mrph.hinsi == '動詞'))
