from pyknp import Juman

from util.reader import get_data

juman = Juman()

for datum in get_data():
    result = juman.result(datum)
    for index, mrph in enumerate(result.mrph_list()):
        if mrph.bunrui == 'サ変名詞' and result[index + 1].genkei in ['する', 'できる', '出来る']:
            print('{} {}'.format(mrph.midasi, result[index + 1].genkei))
