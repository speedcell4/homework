# coding: utf-8
import sys

from pyknp import KNP

knp = KNP()

for line in sys.stdin:
    result = knp.parse(line)
    for bnst in result.bnst_list():
        print(''.join(mrph.midasi for mrph in bnst.mrph_list()), end=' ')
    print()
