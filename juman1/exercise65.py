from typing import List

from pyknp import Juman, Morpheme

from util.reader import get_data

juman = Juman()

predicates = ['動詞', '形容詞']


def percentage(mrph_list: List[Morpheme]) -> float:
    total, n_predicate = 0, 0
    for mrph in mrph_list:
        total += 1
        if mrph.hinsi in predicates:
            n_predicate += 1
    return n_predicate / total


for datum in get_data():
    print(percentage(juman.result(datum).mrph_list()))
