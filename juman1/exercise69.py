from pyknp import KNP

from util.reader import get_data

knp = KNP()

for datum in get_data():
    result = knp.result(datum)
    for bnst in result.bnst_list():
        if any(mrph.hinsi == '接頭辞' for mrph in bnst.mrph_list()):
            print(''.join(mrph.midasi for mrph in bnst.mrph_list()))
